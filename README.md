# Mac OSx Wifi / Ethernet switcher

This tool is forked from [Albertori](https://gist.github.com/albertbori/1798d88a93175b9da00b) and improved with dongle support.

## How to use
- Copy `[intergrated/dongle]/toggleAirport.sh` to `/Library/Scripts/`
- Run `chmod 755 /Library/Scripts/toggleAirport.sh`
- Copy `com.mine.toggleairport.plist` to `/Library/LaunchAgents/`
- Run `sudo chown root /Library/LaunchAgents/com.mine.toggleairport.plist`
- Run `sudo chmod 755 /Library/LaunchAgents/com.mine.toggleairport.plist`
- Run `sudo launchctl load /Library/LaunchAgents/com.mine.toggleairport.plist` to start the watcher